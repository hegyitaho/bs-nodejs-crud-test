
exports.up = async function(knex) {
  await knex.schema
    .createTable('Users', function (table) {
      table.increments('id').primary();
      table.string('username', 255).notNullable().unique();
      table.string('firstName', 255).notNullable();
      table.string('lastName', 255).notNullable();
    });
};

exports.down = async function() {
  /*noop*/
};
