
exports.up = async function(knex) {
  await knex.schema
    .createTable('Tasks', function (table) {
      table.increments('id').primary();
      table.string('name', 255);
      table.string('description', 255);
      table.datetime('dateTime');
      table.integer('userId').unsigned().notNullable();
      table.foreign('userId').references('Users.id');
    });
};

exports.down = function() {
  /*noop*/
};
