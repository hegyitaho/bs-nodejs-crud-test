const request = require('supertest');
const {app} = require('src/app');
const {db} = require('src/persistence');

describe('tasks', () => {
  let server;
  beforeEach(async () => {
    db.initialize();
    await db.migrate.latest();
    server = app.listen();
    await request(server)
      .post('/users')
      .send(USER1);
  });
  afterEach(async () => {
    server.close();
    await db.destroy();
  });
  test('creating responds with resource when successful', async () => {
    await request(server)
      .post('/users/1/tasks')
      .send(TASK)
      .expect('Content-Type', /json/)
      .expect(201, {...TASK, id: 1, user_id: 1});
  });
  describe('update', () => {
    test('ok on success', async () => {
      await request(server)
        .post('/users/1/tasks')
        .send(TASK);
      await request(server)
        .put('/users/1/tasks/1')
        .send({name: 'newName'})
        .expect(200)
        .expect('Content-Location', /127\.0\.0\.1:\d+\/api\/users\/1\/tasks\/1/);
    });
    test('not found', async () => {
      await request(server)
        .put('/users/1/tasks/1')
        .send({name: 'newName'})
        .expect(404);
    });
  });
  describe('get', () => {
    test('resource on success', async () => {
      await request(server)
        .post('/users/1/tasks')
        .send(TASK);
      await request(server)
        .get('/users/1/tasks/1')
        .expect(200, {
          id: 1,
          name: 'task',
          description: 'description',
          date_time: '2016-06-25 14:25:00}',
          user_id: 1,
        });
    });
    test('not found', async () => {
      await request(server)
        .get('/users/1/tasks/1')
        .expect(404);
    });
  });
  describe('delete', () => {
    test('no content on success', async () => {
      await request(server)
        .post('/users/1/tasks')
        .send(TASK);
      await request(server)
        .del('/users/1/tasks/1')
        .expect(204);
      await request(server)
        .get('/users/1/tasks/1')
        .expect(404);
    });
    test('not found', async () => {
      await request(server)
        .del('/users/1/tasks/1')
        .expect(404);
    });
  });
  describe('get all tasks for user', () => {
    test('returns list of tasks', async () => {
      await request(server)
        .post('/users/1/tasks')
        .send(TASK);
      await request(server)
        .post('/users/1/tasks')
        .send(TASK2);
      await request(server)
        .get('/users/1/tasks')
        .expect(200, [{                                   
          id: 1,                            
          name: 'task',                     
          description: 'description',       
          date_time: '2016-06-25 14:25:00}',
          user_id: 1,                        
        },                                  
        {                                   
          id: 2,                            
          name: 'task2',                    
          description: 'description2',      
          date_time: '2016-06-25 14:25:00}',
          user_id: 1,                        
        }]);
    });
    test('returns empty array', async () => {
      await request(server)
        .get('/users/1/tasks')
        .expect(200, []);
    });
  });
});

const TASK = {name:'task', description: 'description', date_time : '2016-05-25 14:25:00'};
const TASK2 = {name:'task2', description: 'description2', date_time : '2016-05-25 14:25:00'};
const USER1 = User(1);

function User(i) {
  return {
    username: `username${i}`,
    last_name: `lastName${i}`,
    first_name: `firstName${i}`,
  };
}