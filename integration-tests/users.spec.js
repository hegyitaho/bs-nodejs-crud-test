const request = require('supertest');
const {app} = require('src/app');
const {db} = require('src/persistence');

describe('users', () => {
  let server;
  beforeEach(async () => {
    db.initialize();
    await db.migrate.latest();
    server = app.listen();
  });
  afterEach(async () => {
    server.close();
    await db.destroy();
  });
  describe('creating', () => {
    test('responds with resource when successful', async () => {
      await request(server)
        .post('/users')
        .send(USER1)
        .expect('Content-Type', /json/)
        .expect(201, USER1);
    });
    describe('bad request when', () => {
      test('username is empty', async () => {
        await request(server)
          .post('/users')
          .send({last_name: 'lastName1',  first_name: 'firstName1'})
          .expect('Content-Type', /text/)
          .expect(400, 'some fields are empty');
      });
      test('first name is empty', async () => {
        await request(server)
          .post('/users')
          .send({username: 'username1',last_name: 'lastName1'})
          .expect('Content-Type', /text/)
          .expect(400, 'some fields are empty');
      });
      test('last name is empty', async () => {
        await request(server)
          .post('/users')
          .send({username: 'username1',  first_name: 'firstName1'})
          .expect('Content-Type', /text/)
          .expect(400, 'some fields are empty');
      });
    });
    test('responds with 409 to creating the same user', async () => {
      await request(server)
        .post('/users')
        .send(USER1);
      await request(server).post('/users')
        .send(USER1)
        .expect(409);
    });
  });

  describe('get user info', () => {
    beforeEach(async () => {
      await request(server)
        .post('/users')
        .send(USER1);
    });
    test('succesful', async () => {
      await request(server).get('/users/1')
        .expect(200, USER1);
    });
    test('not found', async () => {
      await request(server).get('/users/doesnotexist')
        .expect(404);
    });
  });

  describe('list all users', () => {
    test('empty array when none', async () => {
      await request(server).get('/users')
        .expect(200, []);
    });
    test('lists all users', async () => {
      await request(server)
        .post('/users')
        .send(USER1);
      await request(server)
        .post('/users')
        .send(USER2);
      await request(server).get('/users')
        .expect(200, [USER1, USER2]);
    });
  });

  describe('updating', () => {
    beforeEach(async () => {
      await request(server)
        .post('/users')
        .send(USER1);
    });
    test('returns OK and content location to update', async () => {
      await request(server).put('/users/1')
        .send({first_name: 'newFirstName1'})
        .expect(200)
        .expect('Content-Location', /127\.0\.0\.1:\d+\/api\/users\/1/);
      await request(server).get('/users/1')
        .expect(200, UPDATED_USER1);
    });
    test('not found', async () => {
      await request(server).put('/users/doesnotexist')
        .send({last_name: 'newLastName1', first_name: 'newFirstName1'})
        .expect(404);
    });
  });
});

const USER1 = User(1);
const USER2 = User(2);

const UPDATED_USER1 = {...USER1, first_name: 'newFirstName1'};

function User(i) {
  return {
    username: `username${i}`,
    last_name: `lastName${i}`,
    first_name: `firstName${i}`,
    id: i,
  };
}