module.exports = {toTaskDto, toDate};

function toTaskDto({id, name, description, dateTime, userId}) {
  return ({id, name, description, 'date_time': toTimeStamp(dateTime), 'user_id': userId});
}

function toDate(timeStampDto) {
  // timeStamp: '2016-05-25 14:25:00'
  if (!timeStampDto) {
    return undefined;
  }
  const [date, time] = timeStampDto.split(/\s+/);
  const [year, month, day] = date.split('-');
  const [hour, min, sec] = time.split(':');
  return new Date(year, month, day, hour, min, sec);
}

function toTimeStamp(epoch) {
  const date = new Date(epoch);
  return `${date.getFullYear()}-${pad0(date.getMonth()+1)}-${pad0(date.getDate())}` +
  ' ' + 
  `${pad0(date.getHours())}:${pad0(date.getMinutes())}:${pad0(date.getSeconds())}}`;
}

function pad0(digit = '') {
  return ('0' + digit).slice(-2); 
}