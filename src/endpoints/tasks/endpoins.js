const Router = require('@koa/router');
const {OK, CREATED , NO_CONTENT} = require('http-status-codes');

const {db} = require('src/persistence');
const {toTaskDto, toDate} = require('./utils');
const tasksApi = new Router();

module.exports = {tasksApi};

tasksApi.post('/:userId/tasks', async ctx => {
  const userId = parseInt(ctx.params.userId);
  const {name, description, 'date_time': dateTime} = ctx.request.body;
  const [id] = await db('Tasks')
    .returning('id')
    .insert({name, description, dateTime: toDate(dateTime), userId});   
	
  ctx.response.body = {name, description, 'date_time': dateTime, 'user_id': userId, id};
  ctx.response.status = CREATED;
});

tasksApi.put('/:userId/tasks/:taskId', async ctx => {
  const {userId, taskId} = ctx.params;
  const {name, description, 'date_time': dateTime} = ctx.request.body;
  const success = await db('Tasks')
    .where({id: taskId})
    .update({name, description, dateTime: toDate(dateTime), userId});   
  
  if(!success) {
    ctx.throw(404);
  }

  ctx.set('Content-Location', `${ctx.request.host}/api/users/${userId}/tasks/${taskId}`);
  ctx.response.status = OK;
});

tasksApi.del('/:userId/tasks/:taskId', async ctx => {
  const {userId, taskId} = ctx.params;
  const success = await db('Tasks')
    .where({id: taskId, userId})
    .del();   
  
  if(!success) {
    ctx.throw(404);
  }
	
  ctx.response.status = NO_CONTENT;
});

tasksApi.get('/:userId/tasks/:taskId', async ctx => {
  const {userId, taskId} = ctx.params;
  const task = await db('Tasks')
    .where({id: taskId, userId})
    .first();
  
  if(!task) {
    ctx.throw(404);
  }
  ctx.response.body = toTaskDto(task);
});

tasksApi.get('/:userId/tasks', async ctx => {
  const userId = ctx.params.userId;
  const tasks = (await db('Tasks')
    .where({userId}))
    .map(toTaskDto);
  
  ctx.response.body = tasks;
});

