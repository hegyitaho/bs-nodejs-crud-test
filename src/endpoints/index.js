const {usersApi} = require('./users');
const {tasksApi} = require('./tasks/endpoins');

module.exports = {usersApi, tasksApi};