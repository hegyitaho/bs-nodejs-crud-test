const Router = require('@koa/router');
const {CONFLICT, CREATED, OK} = require('http-status-codes');
const {db} = require('src/persistence');

const usersApi = new Router();

module.exports = {usersApi};

usersApi.post('/', async ctx => {
  const {username, 'first_name': firstName, 'last_name': lastName} = ctx.request.body;
  if([username, firstName, lastName].some(name => !name)){
    ctx.throw(400, 'some fields are empty');
  }
  let [id] = await db('Users')
    .returning('id')
    .insert({username, firstName, lastName})
    .catch(({message}) => {
      if(message.includes('UNIQUE constraint failed')) {
        ctx.throw(CONFLICT);
      }
    });

  ctx.response.body = {username, 'first_name': firstName, 'last_name': lastName, id};
  ctx.response.status = CREATED;
});

usersApi.put('/:id', async ctx => {
  const id = ctx.params.id;
  const {'first_name': firstName, 'last_name': lastName} = ctx.request.body;

  const success = await db('Users')
    .where({id})
    .update({firstName, lastName});

  if(!success) {
    ctx.throw(404);
  }
  ctx.set('Content-Location', `${ctx.request.host}/api/users/${id}`);
  ctx.response.status = OK;
});

usersApi.get('/:id', async ctx => {
  const id = ctx.params.id;
  const user = await db('Users')
    .where({id})
    .first();
  if (!user) {
    ctx.throw(404);
  }
  ctx.body = {username: user.username, 'first_name': user.firstName, 'last_name': user.lastName, id: user.id};
});

usersApi.get('/', async ctx => {
  const users = (await db('Users'))
    .map(({username, firstName, lastName, id}) => ({username, 'first_name': firstName, 'last_name': lastName, id}));
  ctx.body = users;
});
