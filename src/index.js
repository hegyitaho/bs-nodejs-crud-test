require('module-alias/register');

const {migrateToLatest} = require('src/migrate');

const {app} = require('src/app');


migrateToLatest().then(() => app.listen(3000));

