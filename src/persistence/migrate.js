const knexMigrate = require('knex-migrate');

module.exports = {migrateToLatest};

async function migrateToLatest(){ 
  await knexMigrate('up', {}, log);
}

function log({action, migration}) {
  console.log('Doing ' + action + ' on ' + migration);
}