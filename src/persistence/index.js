const {db} = require('./db');
const {migrateToLatest} = require('./migrate');

module.exports = {db, migrateToLatest};