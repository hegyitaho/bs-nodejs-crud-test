const dbConfig = require('../../knexfile');

var db = require('knex')(dbConfig);

module.exports = {db};
