const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Router = require('@koa/router');
const {tasksApi, usersApi} = require('src/endpoints');
const app = new Koa();
app.use(bodyParser());

const restApi = new Router('/api');
restApi.use('/users', usersApi.routes(), usersApi.allowedMethods());
restApi.use('/users', tasksApi.routes(), tasksApi.allowedMethods());


app.use(restApi.routes());
app.use(restApi.allowedMethods());

module.exports = {app};


