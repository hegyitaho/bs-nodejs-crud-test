const development = {
  client: 'sqlite3',
  connection: {
    // filename: './dev.sqlite3',
    filename: ':memory:',
  },
  log: {
    warn(_message) {},
  },
  useNullAsDefault: true,
};

const production = {
  client: 'postgresql',
  connection: {
    database: 'my_db',
    user:     'username',
    password: 'password',
  },
  pool: {
    min: 2,
    max: 10,
  },
};

module.exports = process.env.NODE_ENV == 'production' ?
  production :
  development;
